package ru.tsc.avramenko.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.UUID;

public abstract class AbstractEntity implements Serializable {

    @NotNull
    @Getter
    @Setter
    protected String id = UUID.randomUUID().toString();

}