package ru.tsc.avramenko.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.command.AbstractCommand;
import ru.tsc.avramenko.tm.dto.Domain;
import ru.tsc.avramenko.tm.exception.system.ProcessException;

public abstract class AbstractDataCommand extends AbstractCommand {

    protected static final String FILE_BINARY = "./data.bin";

    protected static final String FILE_BASE64 = "./data.base64";

    public Domain getDomain() {
        @NotNull
        final Domain domain = new Domain();
        domain.setUsers(serviceLocator.getUserService().findAll());
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        System.out.println("[DATA SAVE SUCCESSFUL]");
        return domain;
    }

    public void setDomain(@Nullable Domain domain) {
        if(domain == null) throw new ProcessException();
        serviceLocator.getTaskService().clear();
        serviceLocator.getTaskService().addAll(domain.getTasks());
        serviceLocator.getProjectService().clear();
        serviceLocator.getProjectService().addAll(domain.getProjects());
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().addAll(domain.getUsers());
        serviceLocator.getAuthService().logoutData();
        System.out.println("[DATA LOAD SUCCESSFUL]");
    }

}