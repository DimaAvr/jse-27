package ru.tsc.avramenko.tm.comparator;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.entity.IHasStatus;

import java.util.Comparator;

public class ComparatorByStatus implements Comparator<IHasStatus> {

    @NotNull
    private static final ComparatorByStatus INSTANCE = new ComparatorByStatus();

    private ComparatorByStatus() {
    }

    public static ComparatorByStatus getInstance() {
        return INSTANCE;
    }

    @NotNull
    @Override
    public int compare(@Nullable final IHasStatus o1, @Nullable final IHasStatus o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getStatus().compareTo(o2.getStatus());
    }

}