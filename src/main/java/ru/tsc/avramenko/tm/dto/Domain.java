package ru.tsc.avramenko.tm.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.model.User;

import java.io.Serializable;
import java.util.List;

@Getter
public class Domain implements Serializable {

    @NotNull
    private List<User> users;

    @NotNull
    private List<Project> projects;

    @NotNull
    private List<Task> tasks;

    public void setUsers(@NotNull List<User> users) {
        this.users = users;
    }

    public void setProjects(@NotNull List<Project> projects) {
        this.projects = projects;
    }

    public void setTasks(@NotNull List<Task> tasks) {
        this.tasks = tasks;
    }

}