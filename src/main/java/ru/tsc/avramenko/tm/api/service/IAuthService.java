package ru.tsc.avramenko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.enumerated.Role;

public interface IAuthService {

    @NotNull
    String getCurrentUserId();

    void setCurrentUserId(@NotNull String userId);

    boolean isAuth();

    boolean isAdmin();

    void login(@Nullable String login, @Nullable String password);

    void logout();

    void logoutData();

    void checkRoles(@Nullable Role... roles);

}
